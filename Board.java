public class Board
{
	private Die die1;
	private Die die2;

	private int[][] closedTiles; //tracks the dice roll count (1 dice per [])
    final int MAX_DICE_ROLL_COUNT = 3;
	
	public Board()
	{
		die1 = new Die();
		die2 = new Die();
		this.closedTiles = new int[6][6];
	}

	public boolean playATurn()
	{
		this.die1.roll();
		this.die2.roll();

		//System.out.println(this.die1);
		//System.out.println(this.die2);

		//increment count of the die result
        this.closedTiles[this.die1.getPips() - 1][this.die2.getPips() - 1] += 1;    //the die rolls are +1 to make it the actual value on the arrays
        int currentRollCount = this.closedTiles[this.die1.getPips() - 1][this.die2.getPips() - 1];  //so I don't have to rewrite the long array every time      
        
        System.out.println("The dice combination " + this.die1.getPips() + " and " + this.die2.getPips() + " has been rolled " + currentRollCount + " times.");

		return (currentRollCount >= this.MAX_DICE_ROLL_COUNT); //returns wether the current roll > max roll count

	}

	public String toString()
	{	
		String builder = "";
        builder += "[/](1)(2)(3)(4)(5)(6)\n"; //displays the dice values available horizontally                   
        
        for(int i = 0; i < this.closedTiles.length; i++)
        {   //for row
            builder += "("+ (i+1) +") "; //displays dice values vertically
            for(int j = 0; j < this.closedTiles[i].length; j++)
            {
                //for each column, display the count of each dice roll
                builder += this.closedTiles[i][j] + "  ";
            }
            //add a linebreak before the end
            builder += "\n";
        }        
        
		return builder;
	}
}
