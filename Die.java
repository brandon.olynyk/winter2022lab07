import java.util.Random;
public class Die
{
	private int pips; //formal name for dots on a die
	private Random random; //random object to be set by application class
	
	public Die()
	{
		this.random = new Random();
		this.pips = 1;
	}
	
	public int getPips()
	{
		return this.pips;
	}	
	
	public void roll()
	{	
		this.pips = random.nextInt(6) + 1;
	}
	
	public String toString()
	{
		return "Pips Value: " + this.pips;
	}
}
